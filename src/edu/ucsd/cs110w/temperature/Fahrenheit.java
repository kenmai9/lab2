/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author k1mai, cs110wbp
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		float temp = super.getValue();  // retrieve the temperature to print
		return (String.valueOf(temp) + " F");
	}
	
	@Override
	public Temperature toCelsius() {
		float temp = super.getValue();  // retrieve the temperature to convert
		float converted = (temp - 32)*5/9;     // convert fahrenheit to celsius
		Temperature returnTemp = new Celsius(converted);
		return returnTemp;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp = super.getValue();  // retrieve the temperature to convert
		Temperature returnTemp = new Celsius(temp);
		return returnTemp;
	}
	@Override 
	public Temperature toKelvin() { 
		float temp = super.getValue();
		float converted = (temp-32)*5/9 + 273; //convert celsius to fahrenheit
		Temperature returnTemp = new Kelvin(converted);
		return returnTemp;
	}
}


