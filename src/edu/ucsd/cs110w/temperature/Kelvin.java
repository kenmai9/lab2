/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author k1mai, cs110wbp
 *
 */
public class Kelvin extends Temperature 
{
	public Kelvin(float t) 
	{ 
		super(t); 
	} 
	 
	public String toString() 
	{ 
		float temp = super.getValue();  // retrieve the temperature to print
		return (String.valueOf(temp) + " K");
	} 
	@Override 
	public Temperature toCelsius() { 
		float temp = super.getValue();  // retrieve the temperature to convert
		float converted = (float) (temp - 273);     // convert fahrenheit to celsius
		Temperature returnTemp = new Celsius(converted);
		return returnTemp;
	} 
	@Override 
	public Temperature toFahrenheit() { 
		float temp = super.getValue(); // retrieve the temperature to convert
		float toC = (float) (temp - 273); 
		float converted = toC * 9/5 + 32;    // convert celsius to fahrenheit
		Temperature returnTemp = new Fahrenheit(converted);
		return returnTemp;
	}
	@Override 
	public Temperature toKelvin() { 
		float temp = super.getValue();  // retrieve the temperature to convert
		Temperature returnTemp = new Kelvin(temp);
		return returnTemp;
	}
}
