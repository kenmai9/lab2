/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author k1mai, cs110wbp
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		float temp = super.getValue();  // retrieve the temperature to print
		return (String.valueOf(temp) + " C");
	}
	@Override
	public Temperature toCelsius() {
		float temp = super.getValue();  // retrieve the temperature to convert
		Temperature returnTemp = new Celsius(temp);
		return returnTemp;
	}
	@Override
	public Temperature toFahrenheit() {
		float temp = super.getValue();  // retrieve the temperature to convert
		float converted = temp * 9/5 + 32;     // convert celsius to fahrenheit
		Temperature returnTemp = new Fahrenheit(converted);
		return returnTemp;
	}
	@Override 
	public Temperature toKelvin() { 
		float temp = super.getValue();
		float converted = (float) (temp + 273);    // convert celsius to fahrenheit
		Temperature returnTemp = new Kelvin(converted);
		return returnTemp;
	}
}
